/*funciones botones */
function verExperiencia() {
    var element = document.getElementById('experience');      /*asignación*/
      element.classList.toggle("active");                          /*conmuta elemento */
      document.getElementById('skills').classList.remove("active"); /**desactiva las de la columna*/
}  

function verCapacidades() {
  var element = document.getElementById('skills');
  element.classList.toggle("active");
  document.getElementById('experience').classList.remove("active");
}

function verEstudios() {                         
  var element = document.getElementById('studies');
  element.classList.toggle("active");
  document.getElementById('objectives').classList.remove("active");  /* desactiva resto campos columna */
  document.getElementById('software').classList.remove("active");
  document.getElementById('languages').classList.remove("active");
}

function verObjetivos() {
  var element = document.getElementById('objectives');
  element.classList.toggle("active");
  document.getElementById('studies').classList.remove("active");
  document.getElementById('software').classList.remove("active");
  document.getElementById('languages').classList.remove("active");
}

function verInformatica() {
  var element = document.getElementById('software');
  element.classList.toggle("active");
  document.getElementById('studies').classList.remove("active");
  document.getElementById('objectives').classList.remove("active");
  document.getElementById('languages').classList.remove("active");
}

function verIdiomas() {
  var element = document.getElementById('languages');
  element.classList.toggle("active");
  document.getElementById('studies').classList.remove("active");
  document.getElementById('objectives').classList.remove("active");
  document.getElementById('software').classList.remove("active");
}

/*función imprimir nombre */
function ImprimirNombre() {
  var element = document.getElementById('name').innerHTML;
  alert("Echa un vistazo a la consola ;)");
  for (let i = 0; i < 100; i++) {
    console.log(element); 
  }
}